<?php 
$salarioEmpleado = $_POST['salario'];
$horas = $_POST['horas'];

class Boletapago{
    private $salarionominal;
    private $seguro;
    private $afp;
    private $renta;
    private $totaldescuentos;
    private $horaextras;
    private $totalsalario;

    public function getSalarioNominal():float{
        return $this->salarionominal;
    }	
    public function setSalarioNominal(float $SALARIO){
        $this->salarionominal=$SALARIO;
    }	

    public function getSeguro():float{
        return $this->seguro;
    }	
    public function setSeguro(float $segurosocial){
        $this->seguro=$segurosocial;
    }	
    
    public function getAfp():float{
        return $this->afp;
    }	
    public function setAfp(float $AFP){
        $this->afp=$AFP;
    }	

    public function getRenta():float{
        return $this->renta;
    }	
    public function setRenta(float $RENTA){
        $this->renta=$RENTA;
    }

    public function getTotalDescuentos():float{
        return $this->totaldescuentos;
    }	

    public function setTotalDescuentos(float $TotalDes){
        $this->totaldescuentos=$TotalDes;
    }
 
    public function getHorasExtras():float{
        return $this->horaextras;
    }	

    public function setHorasExtras(float $HORASEXTRAS){
        $this->horaextras=$HORASEXTRAS;
    }

    public function getTotalSalario():float{
        return $this->totalsalario;
    }	

    public function setTotalSalario(float $TOTALSALARIO){
        $this->totalsalario=$TOTALSALARIO;
    }
    ///////////////////////////////////////////////////////////////////////////////////

    ///Funciones
    public function CalcularDescuentoISS(float $ISSS){
        $descuentoISSS = $this->salarionominal * $ISSS ;
        $this->setSeguro($descuentoISSS);
    }

    public function CalcularDescuentoAFP(float $AFP){
        $descuentoAFP = $this->salarionominal * $AFP ;
        $this->setAfp($descuentoAFP);
    }

    public function CalcularTotalDescuentos(){
        $totalDescuento = $this->afp + $this->seguro + $this->renta;
        $this->setTotalDescuentos($totalDescuento);
    }

    public function CalcularRenta(){
        $renta = 0;
        $coutafija = 0;
        $exceso = 0;

        if($this->salarionominal >=0.01 && $this->salarionominal <=472.00){
            $renta = 0;
            $coutafija = 0;
            $retencion = 0;
        }else if($this->salarionominal >=472.01 && $this->salarionominal <=895.24){
            $coutafija = 17.67;
            $retencion = ($this->salarionominal - 472) * 0.1;
            $renta = $retencion + $coutafija;
        }else if($this->salarionominal >=895.25 && $this->salarionominal <=2038.10){
            $coutafija = 60;
            $retencion = $this->salarionominal - 895.25 * 0.2;
            $renta = $retencion + $coutafija;   
        }else{
            $coutafija = 288.57;
            $retencion = $this->salarionominal - 2038.11 * 0.3;
            $renta = $retencion + $coutafija;   
        }
        $this->setRenta($renta);
    }

    public function CalcularHorasExtras(float $HORAS){
        $pagohora = 10;
        $totalpagoHX = $HORAS * $pagohora;
        $this->setHorasExtras($totalpagoHX);
    }
    public function CalcularTotalSalario(){
        $totalsalario = $this->salarionominal + $this->horaextras - $this->totaldescuentos;
        $this->setTotalSalario($totalsalario);
    }
    
}
$empleado = new Boletapago();
$empleado->setSalarioNominal($salarioEmpleado);
$empleado->CalcularDescuentoISS(0.075);
$empleado->CalcularDescuentoAFP(0.035);
$empleado->CalcularHorasExtras($horas);
$empleado->CalcularRenta(); 
$empleado->CalcularTotalDescuentos();
$empleado->CalcularTotalSalario();
echo '
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<title>Programacion</title>

<style>
.col-md-12{
    font-size:35px;
}
</style>
</head>
';
echo '
<body>
<h1 class="text-center mt-4">Adminstracion de boleta de empleado</h1>
  <div class="container mt-3">
    <div class="row">
        <div class="col-md-12">';
        echo "<b>El salario del empleado es de:</b> " , $empleado->getSalarioNominal(), "$";
        echo "</br> <b>El descuento por ISSS es de:</b> ",$empleado->getSeguro(), "$";
        echo "</br> <b>El descuento por AFP es de: </b>" , $empleado->getAfp(), "$";
        echo "</br> <b> El descuento de renta es de:</b> " , $empleado->getRenta(), "$";
        echo "</br> <b> El total de descuentos es de:</b> " , $empleado->getTotalDescuentos(),"$";
        echo "</br> <b> El total de pago por horas extras es de:</b> " , $empleado->getHorasExtras(),"$";
        echo "</br> <b> El sueldo liquido del empleado es de: </b> " , $empleado->getTotalSalario(), "$"; 
        echo '</div>
    </div>
  </div>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>';

?>